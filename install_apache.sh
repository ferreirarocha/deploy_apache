#!/bin/bash
# Instala o apache webserver e sobe a pagina  de teste html 

# Instalar o apache e o git

apt install apache2  -y
apt install git


# Remove a página padrão do apache criada no  var/www/html/index.html
sudo rm /var/www/html/index.html 


# Clona o repositório deploy apache
git clone  https://gitlab.com/ferreirarocha/deploy_apache.git 


# Enviar  o site para  o /var/www/html
sudo cp deploy_apache/site/* /var/www/html/
